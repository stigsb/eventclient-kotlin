package net.zedge.event.schema

import net.zedge.zeppa.event.core.JsonPropertiesSetter
import net.zedge.zeppa.event.core.NotEventProperty
import net.zedge.zeppa.event.core.Properties
import net.zedge.zeppa.event.core.Scope

/**
 * Properties of a User
 *
 * Methods of this class are called with reflection during build target "buildTaxonomy"
 * and makes up the event logging taxonomy of user properties.
 *
 * The key defines a property, and value defines its type whenever a methods calls set(key, value).
 *
 * These properties becomes user properties in amplitude, and columns in the zedge events database.
 */
class UserProperties : JsonPropertiesSetter<UserProperties>(Scope.User) {
    companion object {
        @NotEventProperty @JvmStatic fun of() = UserProperties()
        @NotEventProperty @JvmStatic fun of(context: Properties) = of().context(context)
    }

    /* See EventProperties to get examples of how to create a property */
}
