package net.zedge.event.schema

import net.zedge.zeppa.event.core.EventRepresentation
import net.zedge.zeppa.event.core.Properties

/** Enum definitions of events
 *
 * Log an event like this: Event.CLICK_WALLPAPER.with().
 *
 * We recommend naming events on the form <VERB>_<OBJECT>_<ADVERB> where the verb is in present tense
 *
 * Examples:
 *  CLICK_WALLPAPER
 *  ADD_RINGTONES_TO_LIST
 *  PREVIEW_EDITED_CONTENT
 *
 * Log things that are happening now. Not things that happend before or is going to happen. Log things
 * that happens without user input as passive:
 *
 * Examples:
 *  SHOW_INTERSTITIAL_AD(isActive = false)
 *  SHOW_CONSENT_MESSAGE(isActive = false)
 *
 * If you want to include the intention in the event, try to keep the <VERB> in the same tense for easy search
 *
 * Examples:
 *  FAIL_TO_PLAY_VIDEO(isActive = false)
 *  CLICK_DISCOVER_TO_OPEN_APP_FROM_FILE_ATTACHER
 *  SUCCEED_TO_SHARE_EDITED_CONTENT(isActive = false)
 *
 * @param properties Default properties of the event that will be set unless they are overwritten.
 */
enum class Event(private val properties: Properties) : EventRepresentation {
    /* TODO: Remove these example enums */
    CLICK_WALLPAPER,
    ADD_RINGTONES_TO_LIST,
    PREVIEW_EDITED_CONTENT,
    CLICK_DISCOVER_TO_OPEN_APP_FROM_FILE_ATTACHER,
    SHOW_INTERSTITIAL_AD(isActive = false),
    SHOW_CONSENT_MESSAGE(isActive = false),
    FAIL_TO_PLAY_VIDEO(isActive = false),
    SUCCEED_TO_SHARE_EDITED_CONTENT(isActive = false),
    LOSE_EVENTS_FROM_QUEUE_LIMIT(isActive = false),
    ;

    constructor(isActive: Boolean? = null): this(EventProperties.of().activeEvent(isActive))

    override fun toEvent() = EventProperties.of().event(this).with(properties)

    fun with() = toEvent()
    fun with(context: Properties?) = toEvent().with(context)
}
