package net.zedge.event.schema

import net.zedge.zeppa.event.taxonomy.Taxonomy
import net.zedge.zeppa.event.taxonomy.getTaxonomy
import java.net.URL

fun main(args: Array<String>) {
    val endpoint = System.getenv("TAXMAN_API_ENDPOINT") ?: args[0]
    val url = URL(endpoint)
    val taxonomy = Taxonomy(getTaxonomy(url))
    taxonomy.update(EventProperties(), UserProperties())
}
