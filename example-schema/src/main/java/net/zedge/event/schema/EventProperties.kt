package net.zedge.event.schema

import net.zedge.zeppa.event.core.*
import java.io.Serializable

/**
 * Properties of an Event
 *
 * Methods of this class are called with reflection during build target "buildTaxonomy"
 * and makes up the event logging taxonomy of event properties.
 *
 * The key defines a property, and value defines its type whenever a methods calls set(key, value).
 *
 * These properties becomes event properties in amplitude, and columns in the zedge events database.
*/
class EventProperties: JsonEventProperties<EventProperties>(::of) {
    companion object {
        @NotEventProperty @JvmStatic fun of() = EventProperties()
        @NotEventProperty @JvmStatic fun of(event: Event) = event.with()
        @NotEventProperty @JvmStatic fun of(context: Properties) = of().context(context)
    }

    internal fun event(event: Event) = set(EVENT_KEY, event, Event::class.java, Scope.Envelope)

    fun passiveEvent(passive: Boolean?) = set("passive_event", passive)
    fun activeEvent(active: Boolean?) = passiveEvent(active?.let{!it})

    /* TODO: Remove the example properties below */
    fun enumValue(value: java.time.DayOfWeek) = set("enum", value, java.time.DayOfWeek::class.java)
    fun stringValue(value: String?) = set("string", value)
    fun booleanValue(value: Boolean?) = set("boolean", value)
    fun byteValue(value: Byte?) = set("byte", value)
    fun shortValue(value: Short?) = set("short", value)
    fun intValue(value: Int?) = set("int", value)
    fun nullableLong(value: Long?) = set("long", value)
    fun longValue(value: Long) = set("long", value)
    fun doubleValue(value: Double?) = set("double", value)
    fun floatValue(value: Float?) = set("float", value)
    fun stringArray(value: Collection<String>?) = set("string_array", value?.toTypedArray())
    fun intArray(value: Collection<Int>?) = set("int_array", value?.toIntArray())
    fun stringToIntMap(value: Map<String, Int>) = apply {
        set("string_to_int_keys", value.keys.toTypedArray())
        set("string_to_int_values", value.values.toIntArray())
    }
    fun enumToEnumMap(value: Map<java.time.DayOfWeek, java.time.DayOfWeek>) = apply {
        set("enum_to_enum_keys", value.keys.toTypedArray(), java.time.DayOfWeek::class.java)
        set("enum_to_enum_values", value.values.toTypedArray(), java.time.DayOfWeek::class.java)
    }
    fun enumToIntMap(value: Map<java.time.DayOfWeek, Int>) = apply {
        set("enum_to_int_keys", value.keys.toTypedArray(), java.time.DayOfWeek::class.java)
        set("enum_to_int_values", value.values.toIntArray())
    }
    fun kotlinArray(value: Array<String>) = set("string_array", value)
    fun kotlinArray(value: Array<Boolean>) = set("boolean_array", value.toBooleanArray())
    fun kotlinArray(value: Array<Byte>) = set("byte_array", value.toByteArray())
    fun kotlinArray(value: Array<Short>) = set("short_array", value.toShortArray())
    fun kotlinArray(value: Array<Int>) = set("int_array", value.toIntArray())
    fun kotlinArray(value: Array<Long>) = set("long_array", value.toLongArray())
    fun kotlinArray(value: Array<Float>) = set("float_array", value.toFloatArray())
    fun kotlinArray(value: Array<Double>) = set("double_array", value.toDoubleArray())
    fun nativeArray(value: BooleanArray) = set("boolean_array", value)
    fun nativeArray(value: ByteArray) = set("byte_array", value)
    fun nativeArray(value: ShortArray) = set("short_array", value)
    fun nativeArray(value: IntArray) = set("int_array", value)
    fun nativeArray(value: LongArray) = set("long_array", value)
    fun nativeArray(value: FloatArray) = set("float_array", value)
    fun nativeArray(value: DoubleArray) = set("double_array", value)

    fun envelopeProperty(value: String?) = set("envelope", value, Scope.Envelope)
}
