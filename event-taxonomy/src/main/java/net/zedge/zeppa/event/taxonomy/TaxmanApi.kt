package net.zedge.zeppa.event.taxonomy

import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

fun getTaxonomy(taxmanURL: URL): JSONObject {
    val connection = taxmanURL.openConnection() as HttpURLConnection
    connect(connection)
    val body = connection.inputStream.bufferedReader().use { it.readText() }
    if (connection.responseCode < 200 || 300 <= connection.responseCode) {
        throw RuntimeException("Unable to get taxonomy from taxman API\n$body")
    }
    connection.disconnect()
    return JSONObject(body)
}

private fun connect(connection: HttpURLConnection) {
    System.getenv("TAXMAN_API_KEY")?.let {
        connection.setRequestProperty("apikey", it)
    }
    connection.connect()
}

fun putTaxonomy(taxonomy: JSONObject, taxmanURL: URL) {
    val connection = taxmanURL.openConnection() as HttpURLConnection
    connection.requestMethod = "PUT"
    connection.setRequestProperty("Content-Type", "application/json")
    connection.doOutput = true
    connection.doInput = true
    connect(connection)
    connection.outputStream.bufferedWriter().use {
        it.write(taxonomy.toString())
    }
    if (connection.responseCode < 200 || 201 < connection.responseCode) {
        val body = connection.errorStream.bufferedReader().use { it.readText() }
        val issue = if (connection.contentType.startsWith("application/json")) {
            JSONObject(body).getJSONArray("issues").toString(1)
        } else {
            body
        }
        throw RuntimeException("Unable to update remote taxonomy." +
                "\n${connection.responseMessage}" +
                "\n$issue")
    }
    connection.disconnect()
}

