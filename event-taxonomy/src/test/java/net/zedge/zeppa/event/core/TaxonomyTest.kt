package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.ThriftEnum
import net.zedge.zeppa.event.taxonomy.Taxonomy
import net.zedge.zeppa.event.taxonomy.ThriftTaxonomy
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThatThrownBy
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Test
import java.time.Instant

class TaxonomyTest {

    val timestamp = Instant.ofEpochSecond(1)
    val eventMap = Taxonomy("zedge.android", Taxonomy.Lifecycle.Production, timestamp)

    @Test
    fun testAppIdIsSet() {
        assertThat(eventMap.toJson().getString("appid")).isEqualTo("zedge.android")
    }

    @Test
    fun testParseEnumsWithNewEnumClass() {
        eventMap.addEnum(Animal.Animal::class.java)
        assertThat(eventMap.toJson().getAnimalValue(Animal.DOG)).isBetween(1, 2)
        eventMap.addEnum(Animal::class.java)
        assertThat(eventMap.toJson().getAnimalValue(Animal.FISH)).isEqualTo(3)
    }

    enum class Animal {
        DOG, CAT, FISH;
        enum class Animal {
            DOG, CAT
        }
    }

    private fun JSONObject.getAnimalValue(animal: Any): Int = getAnimal(animal).getInt("value")

    private fun JSONObject.getAnimal(animal: Any): JSONObject {
        return getJSONArray("enums")
                .lookupName(Animal::class.java.simpleName)
                .getJSONArray("fields")
                .lookupName(animal.toString())
    }

    private fun JSONArray.lookupName(name: String) = map { it as JSONObject }
            .first { it.getString("name") == name }

    private fun JSONArray.lookupOptionalName(name: String) = map { it as JSONObject }
            .firstOrNull { it.getString("name") == name }

    @Test
    fun testParseEnumsWhenEnumAlreadyExists() {
        val eventMap = Taxonomy(setDogToValue(4))
        eventMap.addEnum(Animal::class.java)
        assertThat(eventMap.toJson().getAnimalValue(Animal.DOG)).isEqualTo(4)
    }

    private fun setDogToValue(value: Int): JSONObject {
        val eventMap = Taxonomy("android")
        eventMap.addEnum(Animal::class.java)
        val withEnums = eventMap.toJson()
        withEnums.getJSONArray("enums")
                .lookupName(Animal::class.java.simpleName)
                .getJSONArray("fields")
                .lookupName(Animal.DOG.toString())
                .put("value", value)
        return withEnums
    }

    private fun Taxonomy.getProperty(key: String) = toJson().getProperty(key)

    private fun JSONObject.getProperty(key: String) = getJSONArray("properties").lookupName(key)

    private fun JSONObject.getNullableProperty(key: String) = getJSONArray("properties").lookupOptionalName(key)

    @Test
    fun testAllSupportedPropertyTypes() {
        val eventMap = ThriftTaxonomy("android", Taxonomy.Lifecycle.Production, timestamp)
        eventMap.addAllProperties(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        assertThat(taxonomy.getProperty("string").getString("type")).isEqualTo("String")
        assertThat(taxonomy.getProperty("boolean").getString("type")).isEqualTo("Bool")
        assertThat(taxonomy.getProperty("byte").getString("type")).isEqualTo("Int8")
        assertThat(taxonomy.getProperty("short").getString("type")).isEqualTo("Int16")
        assertThat(taxonomy.getProperty("int").getString("type")).isEqualTo("Int32")
        assertThat(taxonomy.getProperty("long").getString("type")).isEqualTo("Int64")
        assertThat(taxonomy.getProperty("float").getString("type")).isEqualTo("Float32")
        assertThat(taxonomy.getProperty("double").getString("type")).isEqualTo("Float64")
        assertThat(taxonomy.getProperty("thrift_enum").getString("type"))
                .isEqualTo("Enum:ThriftEnum")
        assertThat(taxonomy.getProperty("string_array").getString("type"))
                .isEqualTo("Array(String)")
        assertThat(taxonomy.getProperty("int_array").getString("type"))
                .isEqualTo("Array(Int32)")
        assertThat(taxonomy.getProperty("string_to_int_keys").getString("type"))
                .isEqualTo("Array(String)")
        assertThat(taxonomy.getProperty("string_to_int_values").getString("type"))
                .isEqualTo("Array(Int32)")
        assertThat(taxonomy.getProperty("enum_to_enum_keys").getString("type"))
                .isEqualTo("Array(Enum:ThriftEnum)")
        assertThat(taxonomy.getProperty("enum_to_enum_values").getString("type"))
                .isEqualTo("Array(Enum:EnumExtensions)")
        assertThat(taxonomy.getProperty("string_array").getString("type"))
                .isEqualTo("Array(String)")
        assertThat(taxonomy.getProperty("boolean_array").getString("type"))
                .isEqualTo("Array(Bool)")
        assertThat(taxonomy.getProperty("byte_array").getString("type"))
                .isEqualTo("Array(Int8)")
        assertThat(taxonomy.getProperty("short_array").getString("type"))
                .isEqualTo("Array(Int16)")
        assertThat(taxonomy.getProperty("int_array").getString("type"))
            .isEqualTo("Array(Int32)")
        assertThat(taxonomy.getProperty("long_array").getString("type"))
            .isEqualTo("Array(Int64)")
        assertThat(taxonomy.getProperty("double_array").getString("type"))
                .isEqualTo("Array(Float64)")
        assertThat(taxonomy.getProperty("float_array").getString("type"))
                .isEqualTo("Array(Float32)")
        assertThat(taxonomy.getProperty("general_enum").getString("type"))
                .isEqualTo("Enum:GeneralEnum")
        assertThat(taxonomy.getProperty("extended_enum").getString("type"))
                .isEqualTo("Enum:ThriftEnum")
        assertThat(taxonomy.getProperty("enum_array").getString("type"))
                .isEqualTo("Array(Enum:ThriftEnum)")
        val extendedEnumField = taxonomy.getJSONArray("enums")
                .lookupName("ThriftEnum")
                .getJSONArray("fields")
        assertThat(extendedEnumField.lookupName("FIRST").getInt("value")).isEqualTo(1)
        assertThat(extendedEnumField.lookupName("SECOND").getInt("value")).isEqualTo(2)
        assertThat(extendedEnumField.lookupName("THIRD").getInt("value")).isEqualTo(3)
    }

    @Test
    fun testUnsupportedEnumOverload() {
        assertThatThrownBy {
            eventMap.addAllProperties(UnsupportedOverloadOfEnums())
        }.hasMessageStartingWith("Cannot change property \"general_enum\" from Enum:")
                .hasMessageContaining("Extend the enum type of Enum:")
    }

    @Test
    fun testChangingFromEnumToString() {
        eventMap.addAllProperties(SupportedPropertyTypes())
        eventMap.addAllProperties(ChangingEnumToString())
    }

    @Test
    fun testAddPropertyOfNewType() {
        val eventMap = Taxonomy(changePropertyType("string", "Int32"))

        val properties = SupportedPropertyTypes()
        val artistId = properties.javaClass.getDeclaredMethod("stringValue", String::class.java)
        eventMap.addProperties(artistId, properties)

        assertThat(eventMap.getProperty("string").getString("type")).isEqualTo("String")
    }

    private fun changePropertyType(key: String, type: String): JSONObject {
        val eventMap = Taxonomy("android", Taxonomy.Lifecycle.Production, timestamp)
        eventMap.addAllProperties(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        taxonomy.getJSONArray("properties").lookupName(key).put("type", type)
        return taxonomy
    }

    @Test
    fun testEnvelopePropertiesAreAdded() {
        eventMap.addAllProperties(SupportedPropertyTypes(Scope.User))
        assertThat(eventMap.getProperty("envelope").getEnum(Scope::class.java, "scope"))
                .isEqualTo(Scope.Envelope)
        assertThat(eventMap.getProperty("long")
                .getEnum(Scope::class.java, "scope")).isEqualTo(Scope.User)
    }

    @Test
    fun testUserPropertyOfWrongType() {
        val eventMap = Taxonomy(changePropertyType("boolean", "Int32"))
        val properties = SupportedPropertyTypes()
        val nullableBoolean = java.lang.Boolean::class.java
        val offensive = properties.javaClass.getDeclaredMethod("booleanValue", nullableBoolean)
        eventMap.addProperties(offensive, properties)
        assertThat(eventMap.getProperty("boolean").getString("type")).isEqualTo("Bool")
    }

    @Test
    fun testUserProperty_ChangingBetweenUserAndEnvelopeScope_isAllowed() {
        eventMap.addAllProperties(SupportedPropertyTypes(Scope.User))

        val properties = SupportedPropertyTypes(Scope.Envelope)
        val string = properties.javaClass.getDeclaredMethod("stringValue", String::class.java)
        eventMap.addProperties(string, properties)
        assertThat(eventMap.toJson()
                .getJSONArray("properties")
                .lookupName("string")
                .getEnum(Scope::class.java, "scope")).isEqualTo(Scope.Envelope)
    }

    @Test
    fun testUserProperty_ChangingBetweenEventAndEnvelopeScope_isNotAllowed() {
        eventMap.addAllProperties(SupportedPropertyTypes(Scope.Event))
        val properties = SupportedPropertyTypes(Scope.User)
        val method = properties.javaClass.getDeclaredMethod("longValue", Long::class.java)
        assertThatThrownBy { eventMap.addProperties(method, properties)}
                .hasMessage("Property \"long\" cannot be a user property" +
                " because it is, or has been, an event property." +
                " Consider choosing a different property name.")
    }

    @Test
    fun testLifecycle() {
        eventMap.update(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        val string = taxonomy.getJSONArray("properties").lookupName("string")
        assertThat(string.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(string.get("last_modified")).isEqualTo(timestamp)
    }

    @Test
    fun testScopeChange()
    {
        eventMap.addAllProperties(SupportedPropertyTypes(Scope.User))
        val secondTime = Instant.ofEpochSecond(2)
        val secondEventMap = Taxonomy(eventMap.toJson(), Taxonomy.Lifecycle.Production, secondTime)
        secondEventMap.update(SupportedPropertyTypes(Scope.Envelope))
        val updatedZid = secondEventMap.toJson().getJSONArray("properties").lookupName("string")
        assertThat(updatedZid.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(updatedZid.get("last_modified")).isEqualTo(secondTime)
    }

    @Test
    fun testDeletingProductionProperty()
    {
        val taxonomy = changePropertyName("thrift_enum", "old_thrift_enum")
        val secondTime = Instant.ofEpochSecond(2)
        val secondEventMap = Taxonomy(taxonomy, Taxonomy.Lifecycle.Production, secondTime)
        secondEventMap.update(SupportedPropertyTypes())
        val secondTaxonomy = secondEventMap.toJson()

        val deleted = secondTaxonomy.getProperty("old_thrift_enum")
        assertThat(deleted.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.ProductionEndOfLife)
        assertThat(deleted.get("last_modified")).isEqualTo(secondTime)

        val kept = secondTaxonomy.getProperty("long")
        assertThat(kept.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(kept.get("last_modified")).isEqualTo(timestamp)

        val properties = secondTaxonomy.getJSONArray("properties")
        properties.lookupName("thrift_enum").put("name", "moved_thrift_enum")
        properties.lookupName("old_thrift_enum").put("name", "thrift_enum")

        val thirdTime = Instant.ofEpochSecond(3)
        val thirdEventMap = Taxonomy(secondTaxonomy, Taxonomy.Lifecycle.Production, thirdTime)
        thirdEventMap.update(SupportedPropertyTypes())

        val restored = thirdEventMap.getProperty("thrift_enum")
        assertThat(restored.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(restored.get("last_modified")).isEqualTo(thirdTime)
    }

    private fun changePropertyName(key: String, name: String, scope: Scope = Scope.Event): JSONObject {
        val eventMap = Taxonomy("android", Taxonomy.Lifecycle.Production, timestamp)
        eventMap.update(SupportedPropertyTypes(scope))
        val taxonomy = eventMap.toJson()
        taxonomy.getJSONArray("properties").lookupName(key).put("name", name)
        return taxonomy
    }

    @Test
    fun testDeletedThenRestoredEnum()
    {
        eventMap.addEnum(Animal::class.java)
        eventMap.deleteUnusedValues()
        val taxonomy = eventMap.toJson()
        val fish = taxonomy.getAnimal(Animal.FISH)
        assertThat(fish.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(fish.get("last_modified")).isEqualTo(timestamp)

        val nextTime = Instant.ofEpochSecond(2)
        val secondEventMap = Taxonomy(taxonomy, Taxonomy.Lifecycle.Production, nextTime)
        secondEventMap.addEnum(Animal.Animal::class.java)
        secondEventMap.deleteUnusedValues()

        val secondTaxonomy = secondEventMap.toJson()
        val deleted = secondTaxonomy.getAnimal(Animal.FISH)
        assertThat(deleted.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.ProductionEndOfLife)
        assertThat(deleted.get("last_modified")).isEqualTo(nextTime)

        val kept = secondTaxonomy.getAnimal(Animal.DOG)
        assertThat(kept.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(kept.get("last_modified")).isEqualTo(timestamp)

        val thirdTime = Instant.ofEpochSecond(3)
        val thirdEventMap = Taxonomy(secondTaxonomy, Taxonomy.Lifecycle.Production, thirdTime)
        thirdEventMap.addEnum(Animal::class.java)
        thirdEventMap.deleteUnusedValues()

        val restored = secondTaxonomy.getAnimal(Animal.FISH)
        assertThat(restored.get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
        assertThat(restored.get("last_modified")).isEqualTo(thirdTime)
    }

    @Test
    fun testNewPreProductionValue() {
        val eventMap = Taxonomy("android", Taxonomy.Lifecycle.PreProduction)
        eventMap.update(SupportedPropertyTypes())
        assertThat(eventMap.getProperty("string").get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.PreProduction)
    }

    @Test
    fun testChangingToPreProductionValueFromDeletedProperty() {
        eventMap.update(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        taxonomy.getProperty("string").put("lifecycle", Taxonomy.Lifecycle.ProductionEndOfLife)
        val preProd = Taxonomy(taxonomy, Taxonomy.Lifecycle.PreProduction)
        preProd.update(SupportedPropertyTypes())
        assertThat(preProd.getProperty("string").get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
    }

    @Test
    fun testChangingToPreProductionValueFromDeletedEnumField() {
        eventMap.addEnum(Animal::class.java)
        val taxonomy = eventMap.toJson()
        taxonomy.getAnimal("DOG").put("lifecycle", Taxonomy.Lifecycle.ProductionEndOfLife)
        val preProd = Taxonomy(taxonomy, Taxonomy.Lifecycle.PreProduction)
        preProd.addEnum(Animal::class.java)
        assertThat(preProd.getAnimal("DOG").get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.Production)
    }

    private fun Taxonomy.getAnimal(animal: String) = toJson().getAnimal(animal)

    @Test
    fun testChangingFromPreProductionToProduction() {
        val preProd = Taxonomy("android", Taxonomy.Lifecycle.PreProduction)
        preProd.update(SupportedPropertyTypes())
        val release = Taxonomy(preProd.toJson(), Taxonomy.Lifecycle.Production)
        release.update(SupportedPropertyTypes())
        assertThat(release.getProperty("string").get("lifecycle"))
                .isEqualTo(Taxonomy.Lifecycle.Production)
    }

    @Test
    fun testChangingFromProductionToPreProduction() {
        val prod = Taxonomy("android", Taxonomy.Lifecycle.Production)
        prod.update(SupportedPropertyTypes())
        val preProd = Taxonomy(prod.toJson(), Taxonomy.Lifecycle.PreProduction)
        preProd.update(SupportedPropertyTypes())
        assertThat(preProd.getProperty("string").get("lifecycle"))
                .isEqualTo(Taxonomy.Lifecycle.Production)
    }

    @Test
    fun testChangingFromPreProductionToPreProduction() {
        val firstTime = Instant.ofEpochSecond(2)
        val prod = Taxonomy("android", Taxonomy.Lifecycle.PreProduction, firstTime)
        prod.update(SupportedPropertyTypes())
        val secondTime = Instant.ofEpochSecond(3)
        val preProd = Taxonomy(prod.toJson(), Taxonomy.Lifecycle.PreProduction, secondTime)
        preProd.update(SupportedPropertyTypes())
        assertThat(preProd.getProperty("string").get("lifecycle")).isEqualTo(Taxonomy.Lifecycle.PreProduction)
        assertThat(preProd.getProperty("string").get("last_modified")).isEqualTo(firstTime)
    }

    @Test
    fun testRemovingPropertyFromPreProduction() {
        eventMap.addAllProperties(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        taxonomy.getJSONArray("properties").lookupName("thrift_enum")
                .put("name", "preprod_lifecycle")
                .put("lifecycle", Taxonomy.Lifecycle.PreProduction)
        val secondTime = Instant.ofEpochSecond(3)
        val prod = Taxonomy(taxonomy, Taxonomy.Lifecycle.Production, secondTime)
        prod.update(SupportedPropertyTypes())
        val updated = prod.toJson()
        assertThat(updated.getNullableProperty("preprod_lifecycle")).isNull()
    }

    @Test
    fun testRemovingEnumFromPreProduction() {
        eventMap.addAllProperties(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        taxonomy.getJSONArray("enums").lookupName("GeneralEnum").getJSONArray("fields").put(
                JSONObject()
                        .put("name", "REMOVED_ENUM_VALUE")
                        .put("lifecycle", Taxonomy.Lifecycle.PreProduction)
                        .put("value", 999))
        val secondTime = Instant.ofEpochSecond(3)
        val prod = Taxonomy(taxonomy, Taxonomy.Lifecycle.Production, secondTime)
        prod.update(SupportedPropertyTypes())
        val updated = prod.toJson()
        assertThat(updated.getJSONArray("enums").lookupName("GeneralEnum").getJSONArray("fields")
                .lookupOptionalName("REMOVED_ENUM_VALUE")).isNull()
    }

    @Test
    fun testUnknownInternalScopedProperty() {
        eventMap.addAllProperties(SupportedPropertyTypes())
        val taxonomy = eventMap.toJson()
        taxonomy.getJSONArray("properties").lookupName("string")
                .put("scope", Scope.Internal)
                .put("name", "internal_property_name")
        val release = Taxonomy(taxonomy)
        release.update(SupportedPropertyTypes())
        assertThat(release.getProperty("internal_property_name").get("scope"))
                .isEqualTo(Scope.Internal)
    }

    @Test
    fun testChangingFromInternalToEventProperty() {
        eventMap.addAllProperties(SupportedPropertyTypes(Scope.Internal))
        val properties = SupportedPropertyTypes(Scope.Event)
        val artistId = properties.javaClass.getDeclaredMethod("thriftEnum", ThriftEnum::class.java)
        assertThatThrownBy { eventMap.addProperties(artistId, properties) }
                .hasMessage("Property \"thrift_enum\" cannot be an event property" +
                " because it is, or has been, an internal property." +
                " Consider choosing a different property name.")
    }

    @Test
    fun testDeletingEnvelopeProperty() {
        val release = Taxonomy(changePropertyName("string", "deleted", Scope.Envelope))
        release.update(SupportedPropertyTypes(Scope.Envelope))
        assertThat(release.toJson().getProperty("deleted").get("lifecycle"))
                .isEqualTo(Taxonomy.Lifecycle.Production)
    }

    @Test
    fun testThriftEnumsAreAddedWithTheirValueIncludingTheZeroValue() {
        val eventMap = ThriftTaxonomy("android", Taxonomy.Lifecycle.Production, timestamp)
        eventMap.addAllProperties(SupportedPropertyTypes())
        val constants = eventMap.toJson()
                .getJSONArray("enums")
                .lookupName("ThriftEnum")
                .getJSONArray("fields")
        assertThat(constants.lookupName("FIRST").getInt("value")).isEqualTo(1)
        assertThat(constants.lookupName("SECOND").getInt("value")).isEqualTo(2)
        assertThat(constants.toString()).contains("ZERO")
    }
    @Test
    fun testOnlyUsedEnumsAreAdded() {
        eventMap.update(SupportedPropertyTypes())
        assertThat(eventMap.toJson().toString()).doesNotContain("NOT_USED")
        assertThat(eventMap.toJson().toString()).contains("FIRST")
    }

}

