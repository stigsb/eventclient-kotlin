package net.zedge.zeppa.event.core

class ChangingEnumToString : JsonPropertiesSetter<ChangingEnumToString>(Scope.Event) {
    fun generalEnum(stringValue: String) = set("general_enum", stringValue)
}
