package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.EventLogger
import net.zedge.zeppa.event.core.LoggableEvent
import net.zedge.zeppa.event.core.Properties

class EventLoggerRecorder: EventLogger {
    val events = mutableListOf<LoggableEvent>()
    val userProperties = mutableListOf<Properties>()
    val types = mutableListOf<EventLoggerRecorder.Type>()

    enum class Type {EVENT, USER_PROPERTY}

    @Synchronized
    override fun log(event: LoggableEvent) {
        events.add(event)
        types.add(Type.EVENT)
    }

    @Synchronized
    override fun identifyUser(properties: Properties) {
        userProperties.add(properties)
        types.add(Type.USER_PROPERTY)
    }

    fun clear() {
        events.clear()
        userProperties.clear()
        types.clear()
    }
}
