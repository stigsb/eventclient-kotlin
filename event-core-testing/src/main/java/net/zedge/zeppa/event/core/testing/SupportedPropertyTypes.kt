package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.JsonPropertiesSetter
import net.zedge.zeppa.event.core.Scope
import org.apache.thrift.TEnum

class SupportedPropertyTypes(scope: Scope = Scope.Event)
    : JsonPropertiesSetter<SupportedPropertyTypes>(scope) {
    companion object { @JvmStatic fun of() = SupportedPropertyTypes() }
    fun stringValue(value: String?) = set("string", value)
    fun booleanValue(value: Boolean?) = set("boolean", value)
    fun byteValue(value: Byte?) = set("byte", value)
    fun shortValue(value: Short?) = set("short", value)
    fun intValue(value: Int?) = set("int", value)
    fun nullableLong(value: Long?) = set("long", value)
    fun longValue(value: Long) = set("long", value)
    fun doubleValue(value: Double?) = set("double", value)
    fun floatValue(value: Float?) = set("float", value)
    fun thriftEnum(value: ThriftEnum) = set("thrift_enum", value)
    fun nullableThriftEnum(value: ThriftEnum?) = set("thrift_enum", value)
    fun stringArray(value: Collection<String>?) = set("string_array", value?.toTypedArray())
    fun intArray(value: Collection<Int>?) = set("int_array", value?.toIntArray())
    fun stringToIntMap(value: Map<String, Int>) = apply {
        set("string_to_int_keys", value.keys.toTypedArray())
        set("string_to_int_values", value.values.toIntArray())
    }
    fun enumToEnumMap(value: Map<ThriftEnum, EnumExtensions>) = apply {
        set("enum_to_enum_keys", value.keys.toTypedArray(), ThriftEnum::class.java)
        set("enum_to_enum_values", value.values.toTypedArray(), EnumExtensions::class.java)
    }
    fun enumToIntMap(value: Map<ThriftEnum, Int>) = apply {
        set("enum_to_int_keys", value.keys.toTypedArray(), ThriftEnum::class.java)
        set("enum_to_int_values", value.values.toIntArray())
    }
    fun kotlinArray(value: Array<String>) = set("string_array", value)
    fun kotlinArray(value: Array<Boolean>) = set("boolean_array", value.toBooleanArray())
    fun kotlinArray(value: Array<Byte>) = set("byte_array", value.toByteArray())
    fun kotlinArray(value: Array<Short>) = set("short_array", value.toShortArray())
    fun kotlinArray(value: Array<Int>) = set("int_array", value.toIntArray())
    fun kotlinArray(value: Array<Long>) = set("long_array", value.toLongArray())
    fun kotlinArray(value: Array<Float>) = set("float_array", value.toFloatArray())
    fun kotlinArray(value: Array<Double>) = set("double_array", value.toDoubleArray())
    fun nativeArray(value: BooleanArray) = set("boolean_array", value)
    fun nativeArray(value: ByteArray) = set("byte_array", value)
    fun nativeArray(value: ShortArray) = set("short_array", value)
    fun nativeArray(value: IntArray) = set("int_array", value)
    fun nativeArray(value: LongArray) = set("long_array", value)
    fun nativeArray(value: FloatArray) = set("float_array", value)
    fun nativeArray(value: DoubleArray) = set("double_array", value)
    fun enumArray(value: List<ThriftEnum>) = set("enum_array", value.toTypedArray(), ThriftEnum::class.java)
    fun extendedEnumArray(value: List<EnumExtensions>) =
            set("enum_array", value.toTypedArray(), EnumExtensions::class.java, ThriftEnum::class.java)

    fun generalEnum(value: GeneralEnum) = set("general_enum", value, GeneralEnum::class.java)
    fun extendedEnum(value: EnumExtensions) =
            set("extended_enum", value, EnumExtensions::class.java, ThriftEnum::class.java)

    fun envelopeProperty(value: String?) = set("envelope", value, Scope.Envelope)
    fun envelopeBoolean(value: Boolean?) = set("envelope_boolean", value, Scope.Envelope)
    fun envelopeBooleanArray(value: BooleanArray?) = set("envelope_boolean_array", value, Scope.Envelope)
    fun incrementInt(increment: Int) = increment("increment_int", increment)
    fun incrementLong(increment: Long) = increment("increment_long", increment)
    fun incrementFloat(increment: Float) = increment("increment_float", increment)
    fun incrementDouble(increment: Double) = increment("increment_double", increment)
    fun appendString(increment: String) = append("append_string", increment)
    fun appendKotlinArray(value: Array<String>) = append("string_array", value)
    fun appendKotlinArray(value: Array<Boolean>) = append("boolean_array", value.toBooleanArray())
    fun appendKotlinArray(value: Array<Byte>) = append("byte_array", value.toByteArray())
    fun appendKotlinArray(value: Array<Short>) = append("short_array", value.toShortArray())
    fun appendKotlinArray(value: Array<Int>) = append("int_array", value.toIntArray())
    fun appendKotlinArray(value: Array<Long>) = append("long_array", value.toLongArray())
    fun appendKotlinArray(value: Array<Float>) = append("float_array", value.toFloatArray())
    fun appendKotlinArray(value: Array<Double>) = append("double_array", value.toDoubleArray())
    fun appendNativeArray(value: BooleanArray) = append("boolean_array", value)
    fun appendNativeArray(value: ByteArray) = append("byte_array", value)
    fun appendNativeArray(value: ShortArray) = append("short_array", value)
    fun appendNativeArray(value: IntArray) = append("int_array", value)
    fun appendNativeArray(value: LongArray) = append("long_array", value)
    fun appendNativeArray(value: FloatArray) = append("float_array", value)
    fun appendNativeArray(value: DoubleArray) = append("double_array", value)
}

enum class GeneralEnum {FIRST, SECOND, THIRD, NAME_WITH_UNDERSCORE}

enum class EnumExtensions(private val value: Int) : TEnum {
    FIRST(1), THIRD(2);
    override fun getValue() = value
}

enum class ThriftEnum(private val value: Int) : TEnum {
    ZERO(0), FIRST(1), SECOND(2);
    override fun getValue() = value
}

enum class NotUsed(private val value: Int) : TEnum {
    NOT_USED(1);
    override fun getValue() = value
}

