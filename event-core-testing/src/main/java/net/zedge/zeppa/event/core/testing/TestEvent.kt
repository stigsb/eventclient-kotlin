package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.EventLogger
import net.zedge.zeppa.event.core.EventRepresentation
import net.zedge.zeppa.event.core.Properties

enum class TestEvent(private val properties: Properties) : EventRepresentation {
    START_APP,
    EVENT_WITH_THRIFT_ENUM(SupportedPropertyTypes.of().thriftEnum(ThriftEnum.FIRST)),
    SUSPEND_APP(isActive = false),
    RESUME_APP(isActive = false),
    RESUME_APP_BULK(isActive = false),
    LOSE_EVENTS_FROM_QUEUE_LIMIT(isActive=false),
    SYNCHRONIZE_EVENT_CLOCK(isActive = false),
    ;

    constructor(isActive: Boolean? = null): this(TestEventProperties.of().activeEvent(isActive))

    override fun toEvent() = TestEventProperties.of().event(this).with(properties)

    fun with() = toEvent()
    fun with(context: Properties?) = toEvent().with(context)

    fun log(logger: EventLogger) = logger.log(this)
}

