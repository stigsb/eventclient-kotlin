#!/usr/bin/env bash
set -e
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 ./gradlew clean javadocJar
./gradlew test assemble
./gradlew uploadArchives
