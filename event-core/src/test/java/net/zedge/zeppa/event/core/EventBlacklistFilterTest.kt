package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.*
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventBlacklistFilterTest {
    val recorder = EventLoggerRecorder()
    val logger = EventBlacklistFilter(setOf(TestEvent.START_APP.name), recorder)

    @Test
    fun testBlacklistingOfEvents() {
        TestEvent.START_APP.with().enumValue(ThriftEnum.SECOND).log(logger)
        val event = TestEvent.SUSPEND_APP.with().enumValue(ThriftEnum.SECOND)
        event.log(logger)
        assertThat(recorder.events.size).isEqualTo(1)
        assertThat(recorder.events[0]).isEqualTo(event)
    }

    @Test
    fun testIdentifyUser() {
        val userProperties = SupportedPropertyTypes(Scope.User).longValue(1)
        logger.identifyUser(userProperties)
        assertThat(recorder.userProperties[0]).isEqualTo(userProperties)
    }
}
