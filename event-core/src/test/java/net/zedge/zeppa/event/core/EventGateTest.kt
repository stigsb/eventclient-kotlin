package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventGateTest {
    private val recorder = EventLoggerRecorder()
    private val logger = loggingEventGate()

    fun loggingEventGate(queueSizeLimit: Int = 1024) = EventGate(queueSizeLimit) { count, logger ->
        TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.with().count(count).log(logger)
    }

    @Test
    fun testBufferingLogs() {
        TestEvent.START_APP.log(logger)
        assertThat(recorder.events).isEmpty()

        logger.open(recorder)
        TestEvent.SUSPEND_APP.log(logger)
        assertThat(recorder.events.size).isEqualTo(2)

        TestEvent.START_APP.log(logger)
        assertThat(recorder.events.size).isEqualTo(3)

        logger.close()
        TestEvent.SUSPEND_APP.log(logger)
        assertThat(recorder.events.size).isEqualTo(3)
        assertThat(recorder.events[0].getEvent()).isEqualTo(TestEvent.START_APP)
        assertThat(recorder.events[1].getEvent()).isEqualTo(TestEvent.SUSPEND_APP)
        assertThat(recorder.events[2].getEvent()).isEqualTo(TestEvent.START_APP)
    }

    private fun LoggableEvent.getEvent(): TestEvent = TestEvent.valueOf(getEventString())

    @Test
    fun testBufferingUserProperties() {
        logger.identifyUser(SupportedPropertyTypes(Scope.User).stringValue("123abc"))
        TestEvent.SUSPEND_APP.log(logger)
        assertThat(recorder.userProperties.size).isEqualTo(0)

        logger.open(recorder)
        logger.identifyUser(SupportedPropertyTypes(Scope.User).booleanValue(true))
        assertThat(recorder.userProperties.size).isEqualTo(2)

        logger.identifyUser(SupportedPropertyTypes(Scope.User).booleanValue(false))
        assertThat(recorder.userProperties.size).isEqualTo(3)

        assertThat(recorder.events[0].getEvent()).isEqualTo(TestEvent.SUSPEND_APP)
        assertThat(recorder.userProperties[0].toProperties()["string"]).isEqualTo("123abc")
        assertThat(recorder.userProperties[1].toProperties()["boolean"]).isEqualTo(true)
        assertThat(recorder.userProperties[2].toProperties()["boolean"]).isEqualTo(false)
        assertThat(recorder.types[0]).isEqualTo(EventLoggerRecorder.Type.USER_PROPERTY)
        assertThat(recorder.types[1]).isEqualTo(EventLoggerRecorder.Type.EVENT)
        assertThat(recorder.types[2]).isEqualTo(EventLoggerRecorder.Type.USER_PROPERTY)
        assertThat(recorder.types[3]).isEqualTo(EventLoggerRecorder.Type.USER_PROPERTY)
    }

    @Test
    fun testLimitingSize() {
        val sizeLimit = 4
        val logger = loggingEventGate(sizeLimit)
        val lostUserProperties = JsonProperties()

        TestEvent.SUSPEND_APP.log(logger)
        val stringUserProperty = SupportedPropertyTypes(Scope.User).stringValue("123abc")
        logger.identifyUser(stringUserProperty)
        lostUserProperties.addAll(stringUserProperty)
        val doubleUserProperty = SupportedPropertyTypes(Scope.User).doubleValue(3.45)
        logger.identifyUser(doubleUserProperty)
        lostUserProperties.addAll(doubleUserProperty)

        // The above events are dropped due to size limit

        TestEvent.START_APP.log(logger)
        val firstUserProperty = SupportedPropertyTypes(Scope.User).booleanValue(false)
        logger.identifyUser(firstUserProperty)
        val secondUserProperty = SupportedPropertyTypes(Scope.User).stringValue("test")
        logger.identifyUser(secondUserProperty)
        TestEvent.SUSPEND_APP.log(logger)
        logger.open(recorder)
        TestEvent.START_APP.log(logger)

        assertThat(recorder.types.size).isEqualTo(5 + 2)
        assertThat(recorder.events[0]).isEqualTo(TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.with().count(1))
        assertThat(recorder.events[1].getEvent()).isEqualTo(TestEvent.START_APP)
        assertThat(recorder.events[2].getEvent()).isEqualTo(TestEvent.SUSPEND_APP)
        assertThat(recorder.events[3].getEvent()).isEqualTo(TestEvent.START_APP)
        assertThat(recorder.userProperties[0]).isEqualTo(lostUserProperties)
        assertThat(recorder.userProperties[1]).isEqualTo(firstUserProperty)
        assertThat(recorder.userProperties[2]).isEqualTo(secondUserProperty)
    }

    @Test
    fun testOpeningTwiceWithLostUserProperties() {
        val logger = loggingEventGate(1)
        val firstUserProperty = SupportedPropertyTypes(Scope.User).stringValue("before first open")
        logger.identifyUser(firstUserProperty)
        logger.log(TestEvent.START_APP)
        logger.open(recorder)
        logger.close()
        val secondUserProperty = SupportedPropertyTypes(Scope.User).intValue(1)
        logger.identifyUser(secondUserProperty)
        logger.log(TestEvent.RESUME_APP)
        logger.open(recorder)
        assertThat(recorder.events[0]).isEqualTo(TestEvent.START_APP.toEvent())
        assertThat(recorder.events[1]).isEqualTo(TestEvent.RESUME_APP.toEvent())
        assertThat(recorder.userProperties[0]).isEqualTo(firstUserProperty)
        assertThat(recorder.userProperties[1]).isEqualTo(secondUserProperty)
    }

    @Test
    fun testReadyShiftingOnAndOff_GivesCorrectOverflowCount() {
        val sizeLimit = 2
        val logger = loggingEventGate(sizeLimit)
        for (i in 1..3) {
            for (j in 1..sizeLimit + i) TestEvent.SUSPEND_APP.log(logger)
            logger.open(recorder)
            TestEvent.START_APP.log(logger)
            assertThat(recorder.events[0]).isEqualTo(TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.with().count(i))
            assertThat(recorder.events.last().getEvent()).isEqualTo(TestEvent.START_APP)
            recorder.clear()
            logger.close()
        }
    }

    @Test
    fun testFlushOnOpen() {
        TestEvent.START_APP.log(logger)
        assertThat(recorder.types).isEmpty()
        logger.open(recorder)
        assertThat(recorder.types.size).isEqualTo(1)
    }
}
