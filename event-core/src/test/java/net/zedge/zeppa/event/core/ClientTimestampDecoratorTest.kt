package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class ClientTimestampDecoratorTest {
    private val recorder = EventLoggerRecorder()
    private var logger = ClientTimestampDecorator(
            logger = recorder,
            timer = TimerGivingIncreasingCounterValue(0L),
            onSynchronize = ::logEvent)

    class TimerGivingIncreasingCounterValue(initialValue: Long) : Timer {
        var counter = initialValue
        override fun elapsedMillisecondsFromReferencePoint(): Long {
            return counter++
        }

        override fun fallbackEpochMillisecondsNow() = 0L
    }

    private fun logEvent(@Suppress("UNUSED_PARAMETER") it: Long) {
        logger.log(TestEvent.SYNCHRONIZE_EVENT_CLOCK)
    }

    @Test
    fun testIdentify() {
        val userProperties = SupportedPropertyTypes.of().stringValue("sid")
        logger.identifyUser(userProperties)
        assertThat(recorder.userProperties[0]).isEqualTo(userProperties)
    }

    @Test
    fun testSizeLimit() {
        logger = ClientTimestampDecorator(
                logger = recorder,
                queueSizeLimit = 3,
                eventOnOverflow = TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.toEvent(),
                timer = TimerGivingIncreasingCounterValue(0L),
                onSynchronize = ::logEvent)

        logger.log(TestEvent.START_APP)
        logger.log(TestEvent.SUSPEND_APP)
        logger.log(TestEvent.RESUME_APP)
        logger.log(TestEvent.SUSPEND_APP)
        logger.synchronizeTime(3000)
        logger.log(TestEvent.START_APP)

        assertThat(recorder.events.size).isEqualTo(5)
        assertThat(recorder.events[0]).isEqualTo(TestEvent.LOSE_EVENTS_FROM_QUEUE_LIMIT.with().count(2).clientTimestamp(3000 - 3))
        assertThat(recorder.events[1]).isEqualTo(TestEvent.RESUME_APP.with().clientTimestamp(3000 - 2))
        assertThat(recorder.events[2]).isEqualTo(TestEvent.SUSPEND_APP.with().clientTimestamp(3000 - 1))
        assertThat(recorder.events[3]).isEqualTo(TestEvent.SYNCHRONIZE_EVENT_CLOCK.with().clientTimestamp(3000 + 1))
        assertThat(recorder.events[4]).isEqualTo(TestEvent.START_APP.with().clientTimestamp(3000 + 2))
    }
}