package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.*
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Before
import org.junit.Test

class EventLoggerHooksTest {
    val hookRecorder = EventHookRecorder<TestEventProperties, SupportedPropertyTypes>()
    val eventRecorder = EventLoggerRecorder()
    val hooks = EventLoggerHooks(eventRecorder)

    @Before
    fun setUp() {
        hooks.add("hook", hookRecorder)
    }

    @Test
    fun testEventHookGetsInvoked() {
        val event = TestEvent.START_APP.with().count(3)
        event.log(hooks)
        hooks.remove("hook")
        assertThat(eventRecorder.events[0]).isEqualTo(event)
        assertThat(hookRecorder.events[0]).isEqualTo(event)
    }

    @Test
    fun testIdentifyUserHookGetsInvoked() {
        val properties = SupportedPropertyTypes(Scope.User).envelopeProperty("abc")
        hooks.identifyUser(properties)
        assertThat(eventRecorder.userProperties[0]).isEqualTo(properties)
        assertThat(hookRecorder.userProperties[0]).isEqualTo(properties)
    }

    @Test
    fun testRemoveByValue() {
        hooks.remove(hookRecorder)
        TestEvent.START_APP.with().count(3).log(hooks)
        assertThat(hookRecorder.events.size).isEqualTo(0)
    }

    @Test
    fun testAlternativeWriting() {
        hooks.add("alternative") {
            hookRecorder.onEvent(it)
        }
        hooks.log(TestEvent.START_APP)
        assertThat(hookRecorder.events[0]).isEqualTo(TestEvent.START_APP.toEvent());
    }
}
