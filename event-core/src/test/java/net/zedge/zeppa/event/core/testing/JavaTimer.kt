package net.zedge.zeppa.event.core.testing

import net.zedge.zeppa.event.core.Timer
import java.time.Instant

class JavaTimer : Timer {
    override fun elapsedMillisecondsFromReferencePoint() = Instant.now().toEpochMilli()
    override fun fallbackEpochMillisecondsNow() = Instant.now().toEpochMilli()
}
