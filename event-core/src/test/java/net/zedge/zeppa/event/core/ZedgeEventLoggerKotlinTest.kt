package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.ZedgeEventLoggerTest.getSerializedEvents
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import net.zedge.zeppa.event.core.testing.ThriftEnum
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Before
import org.junit.Test
import java.util.Collections.addAll

class ZedgeEventLoggerKotlinTest {

    private val server = MockWebServer()
    private lateinit var logger: ZedgeEventLogger

    @Before
    fun setUp() {
        val url = server.url("/api/logsink/")
        logger = ZedgeEventLogger(url.toString(), OkHttpClient.Builder().build(), "release")
    }

    @Test
    fun testEventPropertyTypes() {
        val types = SupportedPropertyTypes()
                .thriftEnum(ThriftEnum.SECOND)
                .intValue(4)
                .stringValue("secret_section")
                .envelopeProperty("top_level")
                .envelopeBoolean(true)
                .stringArray(listOf("first_id"))
                .booleanValue(true)
        logger.log(TestEvent.START_APP.toEvent().apply { addAll(types)})
        val event = getSerializedEvents(server.takeRequest()).events[0]
        assertThat(event.event).isEqualTo(TestEvent.START_APP.name)
        assertThat(event.envelope).isEqualTo("top_level")
        assertThat(event.envelope_boolean).isEqualTo(1.0)
        assertThat(event.properties.size).isEqualTo(6)

        assertThat(event.properties["thrift_enum"]).isEqualTo(ThriftEnum.SECOND.name)
        assertThat(event.properties["int"]).isEqualTo(4.0)
        assertThat(event.properties["string"]).isEqualTo("secret_section")
        assertThat(event.properties["string_array"]).isEqualTo(arrayListOf("first_id"))
        assertThat(event.properties["boolean"]).isEqualTo(1.0)

        assertThat(event.properties["event_dedupe_key"]).isNotNull
    }

    @Test
    fun testLoggingArrayOfEnums() {
        val types = SupportedPropertyTypes().enumToIntMap(mapOf(Pair(ThriftEnum.FIRST, 2)))
        logger.log(TestEvent.RESUME_APP.toEvent().apply { addAll(types)})
        val event = getSerializedEvents(server.takeRequest()).events[0]
        assertThat(event.properties["enum_to_int_keys"]).isEqualTo(arrayListOf(ThriftEnum.FIRST.name))
    }
}
