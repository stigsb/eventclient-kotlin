package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import net.zedge.zeppa.event.core.testing.ThriftEnum
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class PropertyBlacklistFilterTest {
    val recorder = EventLoggerRecorder()
    val logger = PropertyBlacklistFilter(setOf("thrift_enum", "long", "ing", "short", "envelope", "int"), recorder)
    @Test
    fun testBlacklistingProperties() {
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of()
                .thriftEnum(ThriftEnum.FIRST)
                .stringValue("not-filtered")
                .longValue(123L)
                .intValue(3)
                .shortValue(4))
        val copy = event.copy()
        event.log(logger)
        assertThat(recorder.events.size).isEqualTo(1)
        val properties = recorder.events[0].toProperties()
        assertThat(properties.has("thrift_enum")).isFalse()
        assertThat(properties.has("long")).isFalse()
        assertThat(properties.has("int")).isFalse()
        assertThat(properties.has("short")).isFalse()
        assertThat(properties["string"]).isEqualTo("not-filtered")
        assertThat(event).isEqualTo(copy)
    }

    @Test
    fun testIdentifyUser() {
        val userProperties = SupportedPropertyTypes(Scope.User).intValue(1).envelopeProperty("theZid").booleanValue(true)
        val copy = userProperties.copy()
        logger.identifyUser(userProperties)
        assertThat(recorder.userProperties.size).isEqualTo(1)
        assertThat(recorder.userProperties[0]).isEqualTo(SupportedPropertyTypes().booleanValue(true))
        assertThat(userProperties).isEqualTo(copy)
    }
}
