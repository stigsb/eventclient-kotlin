package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.GeneralEnum
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventsSummaryHookTest {
    val bulk = EventsSummaryHook(
            setOf(
                    TestEvent.START_APP.name,
                    TestEvent.RESUME_APP.name))
    val recorder = EventLoggerRecorder()
    @Test
    fun testGetSummaryGivesAllCommonPropertiesAndTheCount() {
        val array = arrayOf("prince")
        bulk.onEvent(
                TestEvent.START_APP.with(SupportedPropertyTypes.of()
                .generalEnum(GeneralEnum.FIRST)
                .stringValue("first")
                .kotlinArray(array)))
        bulk.onEvent(
                TestEvent.START_APP.with(SupportedPropertyTypes.of()
                .generalEnum(GeneralEnum.SECOND)
                .stringValue("second")
                .kotlinArray(array)))
        bulk.onEvent(TestEvent.SUSPEND_APP.with(SupportedPropertyTypes.of().intValue(3)))
        bulk.onEvent(
                TestEvent.RESUME_APP.with(SupportedPropertyTypes.of()
                .generalEnum(GeneralEnum.FIRST)
                .stringValue("second")
                .kotlinArray(array)))

        assertThat(bulk.getSummaryAs(TestEvent.RESUME_APP_BULK))
                .isEqualTo(TestEvent.RESUME_APP_BULK.with(SupportedPropertyTypes.of().kotlinArray(array)).count(3))
    }

    @Test
    fun testBulkDoesNotModifyEvent() {
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("id"))
        val copy = event.copy()
        bulk.onEvent(event)
        assertThat(event).isEqualTo(copy)
    }

    @Test
    fun testSummary() {
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("id"))
        bulk.onEvent(event)
        assertThat(bulk.getSummaryAs(TestEvent.RESUME_APP_BULK))
                .isEqualTo(event.with(TestEvent.RESUME_APP_BULK).count(1))
    }

    @Test
    fun testLogSummaryOnlyLogsWithEventHits() {
        bulk.logSummary(TestEvent.RESUME_APP_BULK, recorder)
        assertThat(recorder.events).isEmpty()

        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("int"))
        bulk.onEvent(event)

        bulk.logSummary(TestEvent.RESUME_APP_BULK, recorder)
        assertThat(recorder.events[0]).isEqualTo(event.with(TestEvent.RESUME_APP_BULK).count(1))
    }

    @Test
    fun testReset() {
        bulk.onEvent(TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("stingray")))
        bulk.reset()
        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().stringValue("dodge"))
        bulk.onEvent(event)
        assertThat(bulk.getSummaryAs(TestEvent.START_APP)).isEqualTo(event.with().count(1))
    }
}
