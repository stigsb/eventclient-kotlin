package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.testing.EventLoggerRecorder
import net.zedge.zeppa.event.core.testing.SupportedPropertyTypes
import net.zedge.zeppa.event.core.testing.TestEvent
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.junit.Test

class EventLoggersTest {
    val recorder = EventLoggerRecorder()
    @Test
    fun testOverwritingLogger() {
        val loggers = EventLoggers()
        val key = "logger_name"
        assertThat(loggers.has(key)).isFalse()
        loggers.put(key, recorder)
        assertThat(loggers.has(key)).isTrue()

        val event = TestEvent.START_APP.with(SupportedPropertyTypes.of().intValue(3))
        event.log(loggers)
        assertThat(recorder.events[0]).isEqualTo(event)

        val secondRecorder = EventLoggerRecorder()
        loggers.put(key, secondRecorder)

        val secondEvent = TestEvent.SUSPEND_APP.with(SupportedPropertyTypes.of().stringValue("theID"))
        secondEvent.log(loggers)
        assertThat(recorder.events.size).isEqualTo(1)
        assertThat(secondRecorder.events[0]).isEqualTo(secondEvent)

        loggers.remove(key)
        TestEvent.RESUME_APP.log(loggers)
        assertThat(secondRecorder.events.size).isEqualTo(1)
    }
}
