package net.zedge.zeppa.event.core

import net.zedge.zeppa.event.core.JsonEventProperties.Companion.EVENT_KEY

class EventMapper(
        private val eventMappings: Map<Any, String> = mapOf(),
        propertyMappings: Map<Properties, String> = mapOf(),
        propertyValueMappings: Map<Properties, String> = mapOf(),
        keyMapping: ((String) -> String)? = null,
        enumMapping: ((String) -> String)? = null,
        logger: EventLogger
) : Mapper(propertyMappings, propertyValueMappings, keyMapping, enumMapping, logger) {

    override fun getKeyMapping(key: String) =
            if (key == EVENT_KEY) null
            else super.getKeyMapping(key)

    override fun getValueMapping(key: String, value: Any) =
            if (key == EVENT_KEY) eventMappings[value] ?: super.getValueMapping(key, value)
            else super.getValueMapping(key, value)
}
