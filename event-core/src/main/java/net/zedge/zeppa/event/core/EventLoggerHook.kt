package net.zedge.zeppa.event.core

interface EventLoggerHook {
    fun onEvent(event: LoggableEvent) {}
    fun onIdentifyUser(properties: Properties) {}
}
