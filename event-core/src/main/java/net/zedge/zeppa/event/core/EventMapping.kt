package net.zedge.zeppa.event.core

interface EventMapping {
    fun getKeyMapping(key: String): String?
    fun getValueMapping(key: String, value: Any): Any?
}
