package net.zedge.zeppa.event.core

import com.amplitude.api.Identify
import org.json.JSONArray
import org.json.JSONObject

open class JsonProperties : Properties {
    protected val envelope = JSONObject()
    protected val increments = JSONObject()
    protected val properties = JSONObject()

    @NotEventProperty
    override fun copy(): Properties = JsonProperties().apply { addAll(this@JsonProperties) }
    @NotEventProperty
    override fun toProperties() = toFlatJson()

    @NotEventProperty
    @Synchronized
    override fun addAll(valuesToOverwrite: Properties) {
        if (valuesToOverwrite is JsonProperties) {
            valuesToOverwrite.envelope.overwriteInto(envelope)
            valuesToOverwrite.properties.overwriteInto(properties)
            valuesToOverwrite.increments.overwriteInto(increments)
        } else {
            throw NotImplementedError("Unsupported properties type " + valuesToOverwrite.javaClass.name)
        }
    }

    protected fun JSONObject.overwriteInto(target: JSONObject) {
        keys().forEach { target.put(it, get(it)) }
    }

    @NotEventProperty
    override fun translate(mapping: EventMapping) = (copy() as JsonProperties).apply {
        envelope.map(mapping)
        properties.map(mapping)
        increments.map(mapping)
    }

    protected fun JSONObject.map(mapping: EventMapping) = keys().asSequence().toList().forEach { key ->
        val value = get(key)
        mapping.getValueMapping(key, value)?.let { put(key, it) }
        mapping.getKeyMapping(key)?.let { put(it, remove(key)) }
    }

    @NotEventProperty
    override fun toFlatJson(): JSONObject {
        val result = JSONObject()
        properties.overwriteInto(result)
        envelope.overwriteInto(result)
        return result
    }

    @NotEventProperty
    override fun toZedgeJson(): JSONObject {
        val result = convertZedgeValues(envelope)
        val zedgeProperties = convertZedgeValues(increments)
        convertZedgeValues(properties).overwriteInto(zedgeProperties)
        result.put("properties", zedgeProperties)
        return result
    }

    @NotEventProperty
    protected fun convertZedgeValues(properties: JSONObject) = JSONObject().apply {
        properties.keys().forEach { key ->
            val value = properties.get(key)
            when {
                value is Boolean -> put(key, mapZedgeBoolean(value))
                value.javaClass.isEnum -> put(key, value.toString())
                value is JSONArray && value.length() > 0 && value.get(0) is Boolean ->
                    put(key, JSONArray(value.toBooleanArray().map(::mapZedgeBoolean)))
                else -> put(key, value)
            }
        }
    }

    private fun JSONArray.toBooleanArray(): BooleanArray {
        val result = BooleanArray(length())
        for (i in 0 until length()) result[i] = get(i) as Boolean
        return result
    }

    private fun mapZedgeBoolean(value: Boolean): Byte = if (value) 1 else 0

    @NotEventProperty
    @Suppress("UNCHECKED_CAST")
    override fun identifyUser(): Identify = toProperties().toIdentify().apply{
        increments.keys().forEachRemaining{ key ->
            when (val value = increments.get(key)) {
                is Int, Short, Byte -> add(key, value as Int)
                is Double -> add(key, value)
                is Float -> add(key, value)
                is Long -> add(key, value)
                is Number -> add(key, value.toDouble())
                is IntArray -> append(key, value)
                is BooleanArray -> append(key, value)
                is FloatArray -> append(key, value)
                is DoubleArray -> append(key, value)
                is Array<*> -> append(key, value as Array<out String>)
                else -> append(key, value.toString())
            }
        }
    }


    @NotEventProperty
    @Suppress("UNCHECKED_CAST")
    private fun JSONObject.toIdentify(): Identify {
        val identification = Identify()
        keys().forEach { key ->
            when (val value = get(key)) {
                is Int, Short, Byte -> identification.set(key, value as Int)
                is Double -> identification.set(key, value)
                is Float -> identification.set(key, value)
                is Long -> identification.set(key, value)
                is Number -> identification.set(key, value.toDouble())
                is IntArray -> identification.set(key, value)
                is BooleanArray -> identification.set(key, value)
                is FloatArray -> identification.set(key, value)
                is DoubleArray -> identification.set(key, value)
                is Array<*> -> identification.set(key, value as Array<out String>)
                else -> {
                    val stringValue = value.toString()
                    if (stringValue.isEmpty()) {
                        identification.unset(key)
                    } else {
                        identification.set(key, stringValue)
                    }
                }
            }
        }
        return identification
    }

    @NotEventProperty
    override fun equals(other: Any?): Boolean {
        if (other is JsonProperties) {
            return toZedgeJson().toString().equals(other.toZedgeJson().toString())
        }
        return false
    }

    @NotEventProperty
    override fun toString(): String {
        return toZedgeJson().toString()
    }

    @NotEventProperty
    override fun removeProperty(property: String) {
        properties.remove(property)
        envelope.remove(property)
        increments.remove(property)
    }

    @NotEventProperty
    @Synchronized
    override fun intersect(other: Properties) {
        if (other is JsonProperties) {
            properties.intersect(other.properties)
            envelope.intersect(other.envelope)
            increments.intersect(other.increments)
        } else {
            throw NotImplementedError("Unsupported intersection with property type: " + other.javaClass.name)
        }
    }
    private fun JSONObject.intersect(properties: JSONObject) {
        keys().asSequence().filter {
            !properties.has(it) || when (get(it)) {
                is JSONArray -> {
                    getJSONArray(it).isEqualTo(properties.getJSONArray(it))

                    false
                }
                else -> properties.get(it) != get(it)
            }
        }.toList().forEach {
            remove(it)
        }
    }

    private fun JSONArray.isEqualTo(rhs: JSONArray?) : Boolean {
        if (rhs == null) return false
        if (length() != rhs.length()) return false
        return !filterIndexed {index, element -> rhs.get(index) != element }.any()
    }
}

