package net.zedge.zeppa.event.core

class PropertyWhitelistFilter(val whitelist: Set<String>, val logger: EventLogger) : EventLogger {
    override fun log(event: LoggableEvent) {
        val result = event.copy()
        filterWhitelistedProperties(result)
        logger.log(result)
    }

    private fun filterWhitelistedProperties(result: Properties) {
        for (property in result.toProperties().keys().asSequence().toSet()) {
            if (property !in whitelist) {
                result.removeProperty(property)
            }
        }
    }

    override fun identifyUser(properties: Properties) {
        val result = properties.copy()
        filterWhitelistedProperties(result)
        logger.identifyUser(result)
    }
}
