package net.zedge.zeppa.event.core

class PropertyBlacklistFilter(private val blacklist: Collection<String>, val logger: EventLogger) : EventLogger {
    override fun log(event: LoggableEvent) {
        val result = event.copy()
        removeBlacklistedProperties(result)
        logger.log(result)
    }

    private fun removeBlacklistedProperties(result: Properties) {
        for (property in blacklist) result.removeProperty(property)
    }

    override fun identifyUser(properties: Properties) {
        val result = properties.copy()
        removeBlacklistedProperties(result)
        logger.identifyUser(result)
    }
}
