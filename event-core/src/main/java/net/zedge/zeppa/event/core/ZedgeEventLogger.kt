package net.zedge.zeppa.event.core

import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class ZedgeEventLogger(
        zedgeLoggingApiUrl: String,
        client: OkHttpClient,
        val build: String)
    : EventLogger {
    private val zedgeLogger: ZedgeLoggerRetrofitService
    internal val executor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    var millisecondsBeforeRetry = 1000
    private var events: JSONArray = JSONArray()

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(zedgeLoggingApiUrl)
                .client(client)
                .callbackExecutor(executor)
                .addConverterFactory(ScalarsConverterFactory.create()).build()

        zedgeLogger = retrofit.create(ZedgeLoggerRetrofitService::class.java)
    }

    override fun log(event: LoggableEvent) {
        addEvent(event.copy().apply {
            setDedupeKey(Random.nextInt(16384).toShort())
        })
        var body = reapEvents()
        ExponentialBackoff(zedgeLogger.log(body, build), millisecondsBeforeRetry).run()
    }

    @Synchronized
    private fun addEvent(event: LoggableEvent) {
        events.put(event.toZedgeJson())
    }

    @Synchronized
    private fun reapEvents(): String {
        val body = JSONObject().put("events", events).toString()
        events = JSONArray()
        return body
    }

    inner class ExponentialBackoff(private val call: Call<String>, private var nextDelay: Int)
        : Runnable, Callback<String> {

        override fun run() {
            call.clone().enqueue(this)
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            if (response.code() in 500..599) retryWithExponentialBackoff()
        }

        fun retryWithExponentialBackoff() {
            executor.schedule(this, nextDelay.toLong(), TimeUnit.MILLISECONDS)
            nextDelay *= 2
        }

        override fun onFailure(call: Call<String>, t: Throwable) {}
    }

    override fun identifyUser(properties: Properties) {
        throw RuntimeException("Expecting user properties to be decorated on the events using UserPropertiesEventDecorator")
    }
}
