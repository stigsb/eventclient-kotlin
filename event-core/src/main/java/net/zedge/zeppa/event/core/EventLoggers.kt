package net.zedge.zeppa.event.core

import java.util.concurrent.ConcurrentHashMap

class EventLoggers(immutableLoggers: Map<String, EventLogger> = emptyMap()) : EventLogger {
    protected val loggers = ConcurrentHashMap<String, EventLogger>(immutableLoggers)

    fun put(key: String, logger: EventLogger) = loggers.put(key, logger)

    fun has(key: String) = loggers.containsKey(key)

    fun remove(key: String) = loggers.remove(key)

    override fun log(event: LoggableEvent) = loggers.forEach { it.value.log(event) }

    override fun identifyUser(properties: Properties) = loggers.forEach { it.value.identifyUser(properties) }
}
