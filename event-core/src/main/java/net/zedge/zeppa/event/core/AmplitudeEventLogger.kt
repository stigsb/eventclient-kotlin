package net.zedge.zeppa.event.core

import com.amplitude.api.AmplitudeClient

class AmplitudeEventLogger(private val client: AmplitudeClient) : EventLogger {

    override fun log(event: LoggableEvent) {
        client.logEvent(event.getEventString(), event.toProperties())
    }

    override fun identifyUser(properties: Properties) {
        client.identify(properties.identifyUser())
    }
}
