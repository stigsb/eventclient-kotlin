package net.zedge.zeppa.event.core

interface LoggableEvent: Properties {
    fun getEventString(): String
    override fun translate(mapping: EventMapping): LoggableEvent

    override fun copy(): LoggableEvent
    fun setDedupeKey(key: Short)
    fun setClientTimestamp(timestamp: Long)
    fun setCount(count: Int)
}
