package net.zedge.zeppa.event.core

class EventWhitelistFilter(private val whiteList: Set<String>, val logger: EventLogger) : EventLogger {
    override fun log(event: LoggableEvent) {
        if (event.getEventString() in whiteList) logger.log(event)
    }

    override fun identifyUser(properties: Properties) {
        logger.identifyUser(properties)
    }
}
