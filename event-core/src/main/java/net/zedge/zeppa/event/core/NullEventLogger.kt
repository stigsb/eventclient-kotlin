package net.zedge.zeppa.event.core

class NullEventLogger : EventLogger {
    override fun log(event: LoggableEvent) {}

    override fun identifyUser(properties: Properties) {}
}
