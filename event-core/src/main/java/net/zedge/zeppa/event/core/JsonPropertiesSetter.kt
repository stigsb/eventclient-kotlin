package net.zedge.zeppa.event.core

import org.apache.thrift.TEnum
import org.json.JSONArray

open class JsonPropertiesSetter<out T>(private val defaultScope: Scope) : JsonProperties() {
    var recorder: PropertyRecorder? = null

    companion object {
        const val ENUM_TYPE = "Enum:"
        const val CLASS_TEMPLATE = "<class>"
        const val ENUM_TYPE_TEMPLATE = "$ENUM_TYPE$CLASS_TEMPLATE"
    }

    interface PropertyRecorder {
        fun record(
                key: String,
                typeTemplate: String,
                scope: Scope,
                enum: Class<*>? = null,
                extend: Class<*>? = null
        )
    }

    @NotEventProperty
    @Synchronized
    protected fun context(properties: Properties?): T {
        properties?.let { addAll(properties) }
        return self()
    }

    @Suppress("UNCHECKED_CAST")
    @NotEventProperty
    protected fun self() = this as T

    @NotEventProperty
    protected fun set(key: String, enum: TEnum?, scope: Scope = defaultScope): T =
            set(key, enum, enum?.javaClass ?: TEnum::class.java, null, scope)

    @NotEventProperty
    protected fun <Enum : Any> set(key: String, enum: Enum?, enumType: Class<Enum>, scope: Scope = defaultScope): T =
            set(key, enum, enumType, null, scope)

    @NotEventProperty
    protected fun <Enum : Any> set(
            key: String,
            enum: Enum?,
            enumType: Class<Enum>,
            extendDefinitionOf: Class<*>? = null,
            scope: Scope = defaultScope): T {
        enum?.let { setScoped(key, enum, scope) }
        recorder?.record(key,
                ENUM_TYPE_TEMPLATE, scope, enumType, extendDefinitionOf)
        return self()
    }

    @NotEventProperty
    protected fun setScoped(key: String, value: Any?, scope: Scope) {
        value?.let {
            if (scope == Scope.Envelope) envelope.put(key, it)
            else properties.put(key, it)
        }
    }

    @NotEventProperty
    protected fun set(key: String, value: String?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "String", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Byte?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Int8", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Boolean?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Bool", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Short?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Int16", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Int?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Int32", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Long?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Int64", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Float?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Float32", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, value: Double?, scope: Scope = defaultScope): T {
        setScoped(key, value, scope)
        recorder?.record(key, "Float64", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: Array<String>?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(String)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: IntArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Int32)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: LongArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Int64)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: BooleanArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Bool)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: ByteArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Int8)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: ShortArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Int16)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: DoubleArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Float64)", scope)
        return self()
    }

    @NotEventProperty
    protected fun set(key: String, values: FloatArray?, scope: Scope = defaultScope): T {
        values?.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array(Float32)", scope)
        return self()
    }

    @NotEventProperty
    protected fun <Enum> set(
            key: String,
            values: Array<Enum>?,
            enumType: Class<Enum>,
            scope: Scope = defaultScope): T {
        values?.map { it.toString() }.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array($ENUM_TYPE_TEMPLATE)", scope, enumType)
        return self()
    }

    @NotEventProperty
    protected fun <Enum> set(
            key: String,
            values: Array<Enum>?,
            enumType: Class<Enum>,
            extendDefinitionOf: Class<*>? = null,
            scope: Scope = defaultScope): T {
        values?.map { it.toString() }.let { setScoped(key, JSONArray(it), scope) }
        recorder?.record(key, "Array($ENUM_TYPE_TEMPLATE)", scope, enumType, extendDefinitionOf)
        return self()
    }

    @NotEventProperty protected fun increment(key: String, value: Int?): T = incrementType(key, value)
    @NotEventProperty protected fun increment(key: String, value: Long?): T = incrementType(key, value)
    @NotEventProperty protected fun increment(key: String, value: Float?): T = incrementType(key, value)
    @NotEventProperty protected fun increment(key: String, value: Double?): T = incrementType(key, value)

    @NotEventProperty
    private fun incrementType(key: String, value: Any?): T {
        value?.let { increments.put(key, it) }
        return self()
    }

    @NotEventProperty protected fun append(key: String, value: String?): T = incrementType(key, value)
    @NotEventProperty protected fun append(key: String, values: Array<String>?): T = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: BooleanArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: ByteArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: ShortArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: IntArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: LongArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: FloatArray?) = incrementArray(key, values)
    @NotEventProperty protected fun append(key: String, values: DoubleArray?) = incrementArray(key, values)

    @NotEventProperty
    private fun incrementArray(key: String, values: Any?): T {
        values?.let { increments.put(key, JSONArray(it)) }
        return self()
    }
}
