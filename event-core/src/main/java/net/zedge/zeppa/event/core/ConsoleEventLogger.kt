package net.zedge.zeppa.event.core

class ConsoleEventLogger(val prefix: String = "") : EventLogger {
    override fun log(event: LoggableEvent) {
        print(event)
    }

    override fun identifyUser(properties: Properties) {
        print(properties)
    }

    private fun print(properties: Properties) {
        val json = properties.toZedgeJson().toString(2)
        for (line in json.split('\n')) {
            println(prefix + line)
        }
    }
}
