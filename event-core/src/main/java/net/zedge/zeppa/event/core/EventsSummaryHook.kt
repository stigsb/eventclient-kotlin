package net.zedge.zeppa.event.core

class EventsSummaryHook(val events: Collection<String>) : EventLoggerHook {
    constructor(vararg event: String) : this(setOf(*event))

    var count = 0
    var commonProperties: LoggableEvent? = null

    override fun onEvent(event: LoggableEvent) {
        if (event.getEventString() in events) {
            record(event)
        }
    }

    @Synchronized
    private fun record(event: LoggableEvent) {
        count += 1
        if (commonProperties == null) {
            commonProperties = event.copy()
        } else {
            commonProperties!!.intersect(event)
        }
    }

    @Synchronized
    fun getSummaryAs(representation: EventRepresentation): LoggableEvent {
        return commonProperties!!.copy().apply {
            addAll(representation.toEvent())
            setCount(count)
        }
    }

    fun logSummary(event: EventRepresentation, logger: EventLogger) {
        if (count > 0) logger.log(getSummaryAs(event))
    }

    @Synchronized
    fun reset() {
        count = 0
        commonProperties = null
    }
}
