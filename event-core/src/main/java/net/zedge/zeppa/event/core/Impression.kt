package net.zedge.zeppa.event.core

import java.util.Locale

class Impression(val ctype: Byte,
                 val itemId: String,
                 val origin: String,
                 val position: Int,
                 var showTimestamp: Long = 0,
                 var hideTimestamp: Long = 0,
                 var maxTimeShown: Long = 0
) {


    // Non-positive delta means it was never hidden. i.e. still visible.
    val impressionTime: Long
        get() {
            var delta = hideTimestamp - showTimestamp
            if (delta <= 0 && showTimestamp > 0) {
                delta = currentTimeMillis() - showTimestamp
            }
            maxTimeShown = Math.max(maxTimeShown, delta)
            return maxTimeShown
        }

    fun updateHideTimestamp() {
        hideTimestamp = currentTimeMillis()
        maxTimeShown = impressionTime
    }

    fun updateShowTimestamp() {
        hideTimestamp = 0
        maxTimeShown = 0
        showTimestamp = currentTimeMillis()
    }

    fun clearTimestamps() {
        hideTimestamp = 0
        showTimestamp = 0
        maxTimeShown = 0
    }

    private fun currentTimeMillis(): Long {
        return System.currentTimeMillis()
    }

    override fun toString(): String {
        return String.format(Locale.ENGLISH,
                "Impression $itemId pos $position visible for $maxTimeShown")
    }
}
