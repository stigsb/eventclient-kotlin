package net.zedge.zeppa.event.core

class UserPropertiesEventDecorator(
        var logger: EventLogger? = null,
        private val userProperties: JsonProperties = JsonProperties()) : EventLogger {
    override fun log(event: LoggableEvent) {
        logger?.log(event.copy().apply {
            addAll(userProperties)
        })
    }

    override fun identifyUser(properties: Properties) {
        userProperties.addAll(properties)
    }

}
