package net.zedge.zeppa.event.core

import java.util.concurrent.ConcurrentHashMap

class EventLoggerHooks(private val logger: EventLogger) : EventLogger {
    private val hooks = ConcurrentHashMap<String, EventLoggerHook>()

    override fun log(event: LoggableEvent) {
        hooks.forEach {
            it.value.onEvent(event)
        }
        logger.log(event)
    }

    override fun identifyUser(properties: Properties) {
        hooks.forEach {
            it.value.onIdentifyUser(properties)
        }
        logger.identifyUser(properties)
    }

    fun add(key: String, hookRecorder: EventLoggerHook) = hooks.put(key, hookRecorder)
    fun add(key: String, hook: (LoggableEvent) -> Unit) = hooks.put(key, HookFromLambda(hook))

    class HookFromLambda(var hook: (LoggableEvent) -> Unit) : EventLoggerHook {
        override fun onEvent(event: LoggableEvent) {
            hook(event)
        }
    }

    fun remove(key: String) = hooks.remove(key)
    fun <T> remove(hook: T): T {
        hooks.filter { it.value === hook }.map { it.key }.firstOrNull()?.let {
            hooks.remove(it)
        }
        return hook
    }
}
