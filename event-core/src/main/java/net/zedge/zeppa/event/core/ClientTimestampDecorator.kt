package net.zedge.zeppa.event.core

import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Setting event.clientTimestamp() on incoming events.
 *
 * Events are queued up until time is synchronized.
 *
 * This tailored to use a Timer like this for android:
 * ```
 *    class AndroidTimer : Timer {
 *      override fun elapsedMillisecondsFromReferencePoint() = SystemClock.elapsedRealtime()
 *      override fun fallbackEpochMillisecondsNow() = System.currentTimeMillis()
 *    }
 * ```
 */
class ClientTimestampDecorator(
        val logger: EventLogger,
        private val timer: Timer,
        queueSizeLimit: Int = 1024,
        private val eventOnOverflow: LoggableEvent? = null,
        private val onSynchronize: (serverTimeNow: Long) -> Unit = {},
        private val onSynchronizeToFallback: (clientTimeNow: Long) -> Unit = {}
) : EventLogger {
    private val timestampDecorator = ClientTimestampQueueDecorator()
    private val gate = eventOnOverflow
            ?.let { LoggingEventGate(it, timestampDecorator, queueSizeLimit) }
            ?: EventGate(queueSizeLimit, timestampDecorator)

    override fun log(event: LoggableEvent) {
        val eventTime = timer.elapsedMillisecondsFromReferencePoint()
        timestampDecorator.bufferNextEventTime(eventTime)
        gate.log(event)
    }

    override fun identifyUser(properties: Properties) {
        logger.identifyUser(properties)
    }

    fun synchronizeTime(serverTimeNow: Long) {
        timestampDecorator.synchronize(serverTimeNow, timer.elapsedMillisecondsFromReferencePoint())
        onSynchronize(serverTimeNow)
        gate.open()
    }

    fun synchronizeToFallbackTime() {
        val fallback = timer.fallbackEpochMillisecondsNow()
        timestampDecorator.synchronize(fallback, timer.elapsedMillisecondsFromReferencePoint())
        onSynchronizeToFallback(fallback)
        gate.open()
    }

    private inner class ClientTimestampQueueDecorator : EventLogger {
        private val durations = ConcurrentLinkedQueue<Long>()

        private var elapsedTimeAtSync: Long? = null
        private var serverTimeAtSync: Long? = null

        @Synchronized
        fun bufferNextEventTime(eventTime: Long) {
            durations.add(eventTime)
            val numEventsProducedWhenBufferDropsEvents = if (eventOnOverflow != null) 1 else 0
            if (durations.size > gate.queueSizeLimit + numEventsProducedWhenBufferDropsEvents) {
                durations.poll()
            }
        }

        @Synchronized
        fun synchronize(serverTimeNow: Long, elapsedMillisecondsNow: Long) {
            serverTimeAtSync = serverTimeNow
            elapsedTimeAtSync = elapsedMillisecondsNow
        }

        override fun log(event: LoggableEvent) {
            val elapsedTimeAtLog = durations.poll()
            val serverTimeNow = serverTimeAtSync!! - elapsedTimeAtSync!! + elapsedTimeAtLog
            logger.log(event.copy().apply { setClientTimestamp(serverTimeNow) })
        }

        override fun identifyUser(properties: Properties) {
            throw NotImplementedError("This operation is currently not in use")
        }
    }
}
