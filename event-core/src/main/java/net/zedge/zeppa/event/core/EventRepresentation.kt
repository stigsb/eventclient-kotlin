package net.zedge.zeppa.event.core

interface EventRepresentation {
    fun toEvent(): LoggableEvent
}
