package net.zedge.zeppa.event.core

import java.util.*

/**
 * Queue up events until #open() is called
 *
 * The events are flushed when open() is called
 *
 * @param onOverflow is called if events are lost, with the number of events.
 */
open class EventGate(val queueSizeLimit: Int = 1024, private val onOverflow: (Int, EventLogger) -> Unit = { _, _ ->}) : EventLogger {
    protected lateinit var logger: EventLogger
    private val events = LinkedList<LoggableEvent>()
    private val userProperties = LinkedList<Properties>()
    enum class Type {EVENT, USER_PROPERTY}
    private val types = LinkedList<Type>()
    private var overflow = 0
    private var overflowedUserProperties : Properties? = null
    @get:Synchronized
    private var open: Boolean = false

    constructor(queueSizeLimit: Int, logger: EventLogger, onOverflow: (Int, EventLogger) -> Unit = { _, _ ->}):
            this(queueSizeLimit, onOverflow) {
        this.logger = logger
    }

    @Synchronized
    fun open(logger: EventLogger) {
        this.logger = logger
        open()
    }

    @Synchronized
    fun open() {
        flush()
        open = true
    }

    @Synchronized
    fun close() {
        open = false
    }

    override fun log(event: LoggableEvent) {
        if (open) logger.log(event)
        else enqueue(event)
    }

    @Synchronized
    private fun enqueue(event: LoggableEvent) {
        events.add(event)
        types.add(Type.EVENT)
        reduceToSizeLimit()
    }

    private fun reduceToSizeLimit() {
        if (types.size > queueSizeLimit) {
            when (types.pollFirst()!!) {
                Type.EVENT -> {
                    events.pollFirst()
                    overflow++
                }
                Type.USER_PROPERTY -> {
                    if (overflowedUserProperties == null) {
                        overflowedUserProperties = userProperties.pollFirst()
                    } else {
                        overflowedUserProperties!!.addAll(userProperties.pollFirst())
                    }
                }
            }
        }
    }

    private fun flush() {
        if (overflow > 0) {
            onOverflow(overflow, logger)
            overflow = 0
        }
        overflowedUserProperties?.let {
            logger.identifyUser(it)
            overflowedUserProperties = null
        }
        types.forEach {
            when (it) {
                Type.EVENT -> logger.log(events.pollFirst())
                Type.USER_PROPERTY -> logger.identifyUser(userProperties.pollFirst())
            }
        }
        types.clear()
    }

    override fun identifyUser(properties: Properties) {
        if (open) logger.identifyUser(properties)
        else enqueue(properties)
    }

    @Synchronized
    private fun enqueue(event: Properties) {
        userProperties.add(event)
        types.add(Type.USER_PROPERTY)
        reduceToSizeLimit()
    }
}
