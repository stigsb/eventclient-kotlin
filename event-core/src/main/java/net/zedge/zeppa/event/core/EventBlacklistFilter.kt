package net.zedge.zeppa.event.core

class EventBlacklistFilter(private val blacklist: Set<String>, val logger: EventLogger) : EventLogger {
    override fun log(event: LoggableEvent) {
        if (event.getEventString() in blacklist) return
        logger.log(event)
    }

    override fun identifyUser(properties: Properties) {
        logger.identifyUser(properties)
    }

}
