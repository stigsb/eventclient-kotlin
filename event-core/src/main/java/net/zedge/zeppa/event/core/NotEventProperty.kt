package net.zedge.zeppa.event.core

/**
 * Event logging taxonomy will not be generated for the annotated method.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER)
annotation class NotEventProperty
