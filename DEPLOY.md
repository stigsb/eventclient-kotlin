- Run `./mavencentral.sh`
- Log in to [sonartype](https://oss.sonatype.org). 
- Choose staging repositories
- Close the repository
- Wait for the repository to close
- Release the version

